<?php

namespace App\Actions;

use App\Services\StorageService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class ServiceStatusAction
{
    public function handle(StorageService $storage)
    {
        return [
            'phpVersion' => PHP_VERSION,
            'laravelVersion' => \Illuminate\Foundation\Application::VERSION,
            'storageSize' => Cache::remember(
                'storageSize',
                now()->addMinutes(config('storage.cache_lifetime')),
                function () use ($storage) {
                    return $storage->getFilesSize(Storage::allFiles(''));
                }
            ),
        ];
    }
}
