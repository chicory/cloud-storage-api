<?php

namespace App\Http\Controllers\v1;

use App\Actions\ServiceStatusAction;
use App\Services\StorageService;
use App\Http\Controllers\Controller;

/**
 * @group Service routes
 *
 * Auxiliary routes for maintenance.
 */
class ServiceStatusController extends Controller
{
    /**
     * Service status.
     *
     * @responseFile 200 storage/responses/ServiceStatusResponse.json
     *
     * @param \App\Actions\ServiceStatusAction $status
     * @param \App\Services\StorageService $storage
     * @return array
     */
    public function __invoke(
        ServiceStatusAction $status,
        StorageService $storage,
    ): array {
        return $status->handle($storage);
    }
}
