<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\v1\CreateUserRequest;
use App\Actions\CreateUserAction;
use App\Http\Resources\v1\UserResource;

/**
 * @group Users endpoinds
 *
 * Endpoints for operations with users.
 */
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except('create');
    }

    /**
     * Create new user.
     *
     * @unauthenticated
     * @responseFile 201 storage/responses/UserResponse.json
     *
     * @param \App\Http\Requests\v1\CreateUserRequest $request
     * @param \App\Actions\CreateUserAction $action
     * @return \App\Http\Resources\v1\UserResource
     */
    public function create(CreateUserRequest $request, CreateUserAction $action): UserResource
    {
        return new UserResource($action->handle($request));
    }

    /**
     * Get current user data.
     *
     * @responseFile 200 storage/responses/UserResponse.json
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\v1\UserResource
     */
    public function current(Request $request): UserResource
    {
        return new UserResource($request->user());
    }
}
