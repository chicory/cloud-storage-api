# Cloud storage API
Simple file storage service on Laravel.

## System requirements
* docker
* docker-compose

## Installation
**Clone repository:**
```
git clone https://codeberg.org/chicory/cloud-storage-api.git
cd cloud-storage-api
```
**Run installation script:**
```
./install.sh
```

## Start/Stop application
**Start:**
```
./vendor/bin/sail up
```
**Stop:**
```
./vendor/bin/sail down
```

## API Documentation
**Generate:**
```
./vendor/bin/sail artisan scribe:generate
```
Documentation will be available at `yourdomain.com/docs`
