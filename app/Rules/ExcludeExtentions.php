<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ExcludeExtentions implements Rule
{
    /**
     * Exclude file extentions.
     *
     * @parmam array<string> File extetions ['php', 'py', 'js' , 'sh'].
     * @return void
     */
    public function __construct(
        private array $exclude,
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($value instanceof UploadedFile) || !$value->isValid()) {
            return false;
        }
        return !in_array($value->getClientOriginalExtension(), $this->exclude);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid file extetion.';
    }
}
