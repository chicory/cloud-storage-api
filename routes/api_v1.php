<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Users endpoinds.
 *
 * Endpoints for operations with users.
 */
Route::controller(\App\Http\Controllers\v1\UserController::class)->group(function () {
    Route::group(['prefix' => 'users'], function () {
        Route::post('', 'create');
        Route::get('current', 'current');
    });
});

/**
 * Access tokens endpoints.
 *
 * Endpoints for operations with access tokens.
 */
Route::controller(\App\Http\Controllers\v1\AccessTokenController::class)->group(function () {
    Route::group(['prefix' => 'tokens'], function () {
        Route::post('', 'create');
    });
});

/**
 * Storage routes.
 *
 * Endpoints for operations with users storage.
 */
Route::controller(\App\Http\Controllers\v1\StorageController::class)->group(function () {
    Route::group(['prefix' => 'files'], function () {
        Route::get('all', 'listAllFiles');
        Route::get('home', 'index');
        Route::get('download', 'download');
        Route::get('directory/{directory}', 'listDirectory');
        Route::post('directory', 'createDirectory');
        Route::post('upload', 'upload');
        Route::patch('rename', 'rename');
        Route::delete('delete', 'delete');
    });
});

/**
 * Service routes.
 *
 * Auxiliary routes for maintenance.
 */
Route::group(['prefix' => 'service'], function () {
    Route::get('status', \App\Http\Controllers\v1\ServiceStatusController::class);
});

/**
 * Public links.
 *
 * Endpoints for public download links.
 */
Route::controller(\App\Http\Controllers\v1\PublicLinkController::class)->group(function () {
    Route::group(['prefix' => 'links'], function () {
        Route::get('', 'index');
        Route::post('', 'create');
        Route::delete('/{link}', 'destroy');
    });
});
