<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class CreateAccessTokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Attempt to authenticate the request's credentials.
     *
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticate(): void
    {
        if (!Auth::attempt($this->only('email', 'password'))) {
            throw ValidationException::withMessages(['auth' => trans('auth.failed')]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'email', 'min:8', 'max:128'],
            'password' => ['required', 'min:8', 'max:128'],
            'tokenName' => ['required', 'min:3', 'max:128'],
        ];
    }

    /**
     * Parameters descriptions for Scribe.
     *
     * @return array<string, mixed>
     */
    public function queryParameters(): array
    {
        return [
            'email' => [
                'description' => 'User\'s E-mail address.',
                'example' => 'john@example.com'
            ],
            'password' => [
                'description' => 'User\'s password.',
                'example' => 'password'
            ],
            'tokenName' => [
                'description' => 'Token name.',
                'example' => 'My Access Token'
            ],
        ];
    }
}
