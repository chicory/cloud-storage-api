<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'path' => ['required', 'string'],
        ];
    }

    /**
     * Parameters descriptions for Scribe.
     *
     * @return array<string, mixed>
     */
    public function queryParameters(): array
    {
        return [
            'path' => [
                'description' => 'Relative path.',
                'example' => '/images/photo.jpg'
            ],
        ];
    }
}
