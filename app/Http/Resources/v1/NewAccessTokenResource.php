<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class NewAccessTokenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->accessToken->id,
            'name' => $this->accessToken->name,
            'createdAt' => $this->accessToken->created_at,
            'expiresAt' => $this->accessToken->expires_at,
            'plainTextToken' => $this->plainTextToken,
        ];
    }
}
