<?php

namespace App\Http\Requests\v1;

use App\Rules\DiskQutaLimiter;
use App\Rules\ExcludeExtentions;
use App\Rules\Filename;
use App\Services\StorageService;
use Illuminate\Foundation\Http\FormRequest;

class FileUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'file' => [
                'required',
                'file',
                'max:'.(config('service.max_file_size') / 1000),
                new Filename('/^[A-Za-z0-9\.\-\_\ ]{3,128}$/'),
                new ExcludeExtentions(['php']),
                new DiskQutaLimiter(config('service.quota'), new StorageService()),
            ],
            'directory' => ['string', 'min:3', 'max:128', 'regex:/^[A-Za-z0-9\.\-\_\ ]{3,128}$/'],
            'deleteAfer' => ['integer', 'min:1', 'max:43800'],
        ];
    }

    /**
     * Parameters descriptions for Scribe.
     *
     * @return array<string, mixed>
     */
    public function queryParameters(): array
    {
        return [
            'file' => [
                'description' => 'File.',
                'example' => null
            ],
            'directory' => [
                'description' => 'Directory name.',
                'example' => 'MyFolder'
            ],
            'deleteAfter' => [
                'description' => 'Delete the file after an interval in minutes.',
                'example' => 30
            ],
        ];
    }
}
