<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\v1\CreateDirectoryRequest;
use App\Http\Requests\v1\FileRequest;
use App\Http\Requests\v1\FileUploadRequest;
use App\Http\Requests\v1\RenameFileRequest;
use App\Jobs\RemoveUserFileJob;
use App\Models\PublicLink;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * File storage matipulation service.
 *
 * @method \Illuminate\Support\Collection makeDirectoryCollection(string $path)
 * @method \Illuminate\Support\Collection makeDirectoriesCollection(string $path)
 * @method \Illuminate\Support\Collection makeFilesCollection(string $path)
 * @method \Illuminate\Support\Collection makeAllFilesCollection(string $path)
 * @method \Illuminate\Support\Collection makeCollection(array $pathArray, string $type)
 * @method array getMetaData(string $path, string $type)
 * @method string lastModified(string $path)
 * @method int getSize(array $files)
 * @method int getFilesSize(array $files)
 * @method bool exists(string $path)
 * @method array createDirectory(\App\Http\Requests\v1\CreateDirectoryRequest $request)
 * @method \Symfony\Component\HttpFoundation\StreamedResponse download(\App\Http\Requests\v1\FileRequest $request)
 * @method void delete(\App\Http\Requests\v1\FileRequest $request)
 * @method array rename(\App\Http\Requests\v1\RenameFileRequest $request)
 * @method array upload(\App\Http\Requests\v1\FileUploadRequest $request)
 */
class StorageService
{
    public function __construct()
    {
    }

    /**
     * Make aggregated collection from path.
     *
     * @param string $path Relative path.
     * @return \Illuminate\Support\Collection
     */
    public function makeDirectoryCollection(string $path): Collection
    {
        return $this->makeDirectoriesCollection($path)
            ->merge($this->makeFilesCollection($path));
    }

    /**
     * Make directories collection from path.
     *
     * @param string $path Relative path.
     * @return \Illuminate\Support\Collection
     */
    public function makeDirectoriesCollection(string $path): Collection
    {
        return $this->makeCollection(Storage::directories($path), 'directory');
    }

    /**
     * Make files collection from path.
     *
     * @param string $path Relative path.
     * @return \Illuminate\Support\Collection
     */
    public function makeFilesCollection(string $path): Collection
    {
        return $this->makeCollection(Storage::files($path), 'file');
    }

    /**
     * Make all files (recursively) collection from path.
     *
     * @param string $path Relative path.
     * @return \Illuminate\Support\Collection
     */
    public function makeAllFilesCollection(string $path): Collection
    {
        return $this->makeCollection(Storage::allFiles($path), 'file');
    }

    /**
     * Make collection.
     *
     * @param string $path Relative path.
     * @return \Illuminate\Support\Collection
     */
    public function makeCollection(array $pathArray, string $type): Collection
    {
        $collection = collect();
        foreach ($pathArray as $path) {
            $collection->push($this->getMetaData($path, $type));
        }
        return $collection;
    }

    /**
     * Get object metadata.
     *
     * @param string $path Relative path.
     * @param string $type Type for metadata.
     * @return array<mixed>
     */
    public function getMetaData(string $path, string $type): array
    {
        return [
            'name' => basename($path),
            'path' => strstr($path, '/'),
            'type' => $type,
            'size' => $this->getSize($path),
            'lastModified' => $this->lastModified($path),
        ];
    }

    /**
     * Get the date and time the object was last modified.
     *
     * @param string $path Relative path.
     * @return string Formated datetime.
     */
    public function lastModified(string $path): string
    {
        return Carbon::createFromTimestamp(Storage::lastModified($path))
            ->toDateTimeString();
    }

    /**
     * Get object (file or directory) size.
     *
     * @param string $path Relative path.
     * @return int Size in bytes.
     */
    public function getSize(string $path): int
    {
        if (is_dir(storage_path('app/'. $path))) {
            return $this->getFilesSize(Storage::allFiles($path));
        }
        return Storage::size($path);
    }

    /**
     * Get files size from an array of paths.
     *
     * @param array<string> $files
     * @return int Size in bytes.
     */
    public function getFilesSize(array $files): int
    {
        $total = 0;
        foreach ($files as $file) {
            $total += Storage::size($file);
        }
        return $total;
    }

    /**
     * Is object exist.
     *
     * @param string $path Relative path.
     * @return bool
     */
    public function exists(string $path): bool
    {
        return Storage::exists($path);
    }

    /**
     * Create directory.
     *
     * @param \App\Http\Requests\v1\CreateDirectoryRequest $request
     * @return array
     */
    public function createDirectory(CreateDirectoryRequest $request): array
    {
        $path = $request->user()->id . '/' . $request->directory;
        if ($this->exists($path)) {
            abort(422, 'Directory exists.');
        }
        if (!Storage::makeDirectory($path)) {
            abort(500, 'Unable to create directory.');
        }
        return $this->getMetaData($path, 'directory');
    }

    /**
     * Download requested file.
     *
     * @param \App\Http\Requests\v1\FileRequest $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function download(FileRequest $request): StreamedResponse
    {
        $path = $request->user()->id . $request->path;
        if ($this->exists($path)) {
            return Storage::download($path);
        }
        abort(404);
    }

    /**
     * Delete requested file or directory.
     *
     * @param \App\Http\Requests\v1\FileRequest $request
     * @return void
     */
    public function delete(FileRequest $request): void
    {
        $path = $request->user()->id . $request->path;
        if ($this->exists($path)) {
            Storage::delete($path);

            PublicLink::deleteByPath(
                $request->user()->id,
                $request->path
            );

            return;
        }
        abort(404);
    }

    /**
     * Rename file or directory.
     *
     * @param \App\Http\Requests\v1\RenameFileRequest $request
     * @return array<mixed>
     */
    public function rename(RenameFileRequest $request): array
    {
        $path = $request->user()->id . $request->path;
        $newPath = substr($path, 0, strrpos($path, '/')). '/' . $request->newName;
        if ($this->exists($path)) {
            Storage::move($path, $newPath);

            PublicLink::updatePath(
                $request->user()->id,
                $request->path,
                strstr($newPath, '/'),
            );

            return $this->getMetaData($newPath, 'renamed');
        }
        abort(404);
    }

    /**
     * Upload file.
     *
     * @param \App\Http\Requests\v1\FileUploadRequest $request
     * @return array<mixed>
     */
    public function upload(FileUploadRequest $request): array
    {
        $path = $request->user()->id . '/';
        $path .= $request->has('directory') ? $request->directory . '/' : '';

        if (!$this->exists($path)) {
            abort(422, 'Invalid directory name.');
        }

        $uploadedPath = str_replace('//', '/', $request->file('file')->storeAs(
            $path,
            $request->file('file')->getClientOriginalName(),
        ));

        if ($request->has('deleteAfter')) {
            RemoveUserFileJob::dispatch(
                $uploadedPath
            )->delay(
                now()->addMinutes($request->deleteAfter)
            );
        }

        if (config('service.create_link')) {
            PublicLink::factory()->create([
                'user_id' => $request->user()->id,
                'path' => strstr($uploadedPath, '/'),
            ]);
        }

        return $this->getMetaData($uploadedPath, 'file');
    }
}
