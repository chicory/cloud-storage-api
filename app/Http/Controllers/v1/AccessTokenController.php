<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\CreateAccessTokenRequest;
use App\Http\Resources\v1\NewAccessTokenResource;

/**
 * @group Access tokens endpoints
 * Endpoints for operations with access tokens.
 */
class AccessTokenController extends Controller
{
    /**
     * Create new access token.
     *
     * @unauthenticated
     * @responseFile 201 storage/responses/NewAccessTokenResponse.json
     *
     * @param \App\Http\Requests\v1\CreateAccessTokenRequest $request
     * @return \App\Http\Resources\v1\NewAccessTokenResource
     */
    public function create(CreateAccessTokenRequest $request): NewAccessTokenResource
    {
        $request->authenticate();
        return new NewAccessTokenResource($request->user()->createToken($request->tokenName));
    }
}
