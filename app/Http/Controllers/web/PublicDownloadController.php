<?php

namespace App\Http\Controllers\web;

use App\Models\PublicLink;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class PublicDownloadController extends Controller
{
    /**
     * Download file.
     *
     * @param string $name
     * @return @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function __invoke(string $name): StreamedResponse
    {
        $link = PublicLink::where('name', $name)->first();
        if (!$link) {
            abort(404);
        }
        return Storage::download($link->user_id . $link->path);
    }
}
