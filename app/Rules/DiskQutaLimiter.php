<?php

namespace App\Rules;

use App\Services\StorageService;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DiskQutaLimiter implements Rule
{
    /**
     * Disk quota check.
     *
     * @param int $quota Disk quota.
     * @param int $quota User id.
     * @param \App\Services\StorageService $storage
     * @return void
     */
    public function __construct(
        private int $qouta,
        private StorageService $storage,
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($value instanceof UploadedFile) || !$value->isValid()) {
            return false;
        }
        $filesSize = $this->storage->getFilesSize(Storage::allFiles((string) Auth::user()->id));
        return $value->getSize() + $filesSize < $this->qouta;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'File upload rejected. Disk quota exceeded.';
    }
}
