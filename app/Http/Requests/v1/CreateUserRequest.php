<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'min:8', 'max:128'],
            'email'=> ['required', 'min:8', 'max:128', 'email', 'unique:users,email'],
            'password' => ['required', 'min:8', 'max:128'],
        ];
    }

    /**
     * Parameters descriptions for Scribe.
     *
     * @return array<string, mixed>
     */
    public function queryParameters(): array
    {
        return [
            'name' => [
                'description' => 'User\'s name.',
                'example' => 'John Doe'
            ],
            'email' => [
                'description' => 'User\'s E-mail adress.',
                'example' => 'john@example.com'
            ],
            'password' => [
                'description' => 'User\'s password.',
                'example' => 'password'
            ],
        ];
    }
}
