<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicLink extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'name',
        'path',
    ];

    /**
     * Update path.
     *
     * @param  int $userId
     * @param  string $path
     * @param  string $updateTo
     * @return int
     */
    public static function updatePath(int $userId, string $path, string $updateTo): int
    {
        return self::where('path', $path)->where('user_id', $userId)->update(['path' => $updateTo]);
    }

    /**
     * Delete by path.
     *
     * @param  int $userId
     * @param  string $path
     * @param  string $updateTo
     * @return int
     */
    public static function deleteByPath(int $userId, string $path)
    {
        return self::where('path', $path)->where('user_id', $userId)->delete();
    }
}
