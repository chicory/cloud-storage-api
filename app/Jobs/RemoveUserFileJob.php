<?php

namespace App\Jobs;

use App\Models\PublicLink;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class RemoveUserFileJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $path;

    /**
     * Remove user's file.
     *
     * @param int $userId
     * @param int $path Relative path.
     * @return void
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (Storage::delete($this->path)) {
            PublicLink::deleteByPath(
                strstr($this->path, '/', true),
                strstr($this->path, '/')
            );
            info('RemoveUserFileJob File ' . $this->path . ' removed.');
        } else {
            info('RemoveUserFileJob File ' . $this->path . ' not found.');
        }
    }
}
