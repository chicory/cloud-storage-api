<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Storage Size Chache Lifetime.
    |--------------------------------------------------------------------------
    |
    | The cache lifetime of the total size of the files in storage, in minutes.
    |
    */
    'cache_lifetime' => env('STORAGE_SIZE_CACHE_TIME', 1),

    /*
    |--------------------------------------------------------------------------
    | Max Uploaded File Size.
    |--------------------------------------------------------------------------
    |
    | Maximum size limit for uploaded files in bytes.
    |
    */
    'max_file_size' => env('STORAGE_MAX_FILE_SIZE', 20000000),

    /*
    |--------------------------------------------------------------------------
    | Disk Quota.
    |--------------------------------------------------------------------------
    |
    | Disk usage quota for user in bytes.
    |
    */
    'quota' => env('STORAGE_QUOTA', 100000000),

    /*
    |--------------------------------------------------------------------------
    | Create link when file uploaded.
    |--------------------------------------------------------------------------
    |
    | Create link when file uploaded. Boolean.
    |
    */
    'create_link' => env('STORAGE_CREATE_LINK', true),
];
