#!/bin/bash
cp .env.example .env
docker run --rm --interactive --tty -v $(pwd):/app composer install
./vendor/bin/sail up -d
echo "Starting up services..."
sleep 10
./vendor/bin/sail artisan key:generate
./vendor/bin/sail artisan migrate
./vendor/bin/sail down
