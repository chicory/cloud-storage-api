<?php

namespace App\Actions;

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\v1\CreateUserRequest;
use Illuminate\Support\Facades\Hash;

class CreateUserAction
{
    /**
     * Create user from request.
     *
     * @param \App\Http\Requests\v1\CreateUserRequest $request
     * @return \App\Models\User
     */
    public function handle(CreateUserRequest $request): User
    {
        $user = User::factory()->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        Storage::makeDirectory($user->id);
        return $user;
    }
}
