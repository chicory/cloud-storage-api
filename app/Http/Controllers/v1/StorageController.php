<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Services\StorageService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\v1\CreateDirectoryRequest;
use App\Http\Requests\v1\FileRequest;
use App\Http\Requests\v1\FileUploadRequest;
use App\Http\Requests\v1\RenameFileRequest;
use App\Http\Resources\v1\StorageItemResource;
use App\Http\Resources\v1\StorageItemCollection;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * @group Storage routes.
 *
 * Endpoints for operations with users storage.
 */
class StorageController extends Controller
{
    public function __construct(
        private StorageService $storage,
    ) {
        $this->middleware('auth:sanctum');
    }

    /**
     * List files in home directory.
     *
     * @responseFile 200 storage/responses/StorageItemCollectionResponse.json
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function index(Request $request): StorageItemCollection
    {
        return new StorageItemCollection($this->storage->makeDirectoryCollection($request->user()->id));
    }

    /**
     * List directory.
     *
     * @responseFile 200 storage/responses/StorageItemCollectionResponse.json
     *
     * @param \Illuminate\Http\Request $request
     * @param string $directory
     * @return array
     */
    public function listDirectory(Request $request, string $directory): StorageItemCollection
    {
        if ($this->storage->exists($request->user()->id . '/' . $directory)) {
            return new StorageItemCollection($this->storage->makeFilesCollection($request->user()->id . '/' . $directory));
        }
        abort(404, 'Directory ' . $directory . ' not fond.');
    }

    /**
     * List all user's files.
     *
     * @responseFile 200 storage/responses/StorageItemCollectionResponse.json
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function listAllFiles(Request $request): StorageItemCollection
    {
        return new StorageItemCollection($this->storage->makeAllFilesCollection($request->user()->id));
    }

    /**
     * Create directory.
     *
     * @responseFile 201 storage/responses/StorageItemResponse.json
     *
     * @param \App\Http\Requests\v1\CreateDirectoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function createDirectory(CreateDirectoryRequest $request): StorageItemResource
    {
        return new StorageItemResource($this->storage->createDirectory($request));
    }

    /**
     * Download file.
     *
     * @response 200 <<binary>> File.
     *
     * @param \App\Http\Requests\v1\FileRequest $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function download(FileRequest $request): StreamedResponse
    {
        return $this->storage->download($request);
    }

    /**
     * Delete file or directory.
     *
     * @response 204
     *
     * @param \App\Http\Requests\v1\FileRequest $request
     * @return Illuminate\Http\Response
     */
    public function delete(FileRequest $request): Response
    {
        $this->storage->delete($request);
        return response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Rename file or directory.
     *
     * @responseFile 200 storage/responses/StorageItemResponse.json
     *
     * @param \App\Http\Requests\v1\RenameFileRequest $request
     * @return \App\Http\Resources\v1\StorageItemResource
     */
    public function rename(RenameFileRequest $request): StorageItemResource
    {
        return new StorageItemResource($this->storage->rename($request));
    }

    /**
     * File upload.
     *
     * @responseFile 200 storage/responses/StorageItemResponse.json
     *
     * @param \App\Http\Requests\v1\FileUploadRequest $request
     * @return \App\Http\Resources\v1\StorageItemResource
     */
    public function upload(FileUploadRequest $request): StorageItemResource
    {
        return new StorageItemResource($this->storage->upload($request));
    }
}
