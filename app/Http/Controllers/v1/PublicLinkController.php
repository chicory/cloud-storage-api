<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\FileRequest;
use App\Http\Resources\v1\PublicLinkCollection;
use App\Http\Resources\v1\PublicLinkResource;
use App\Models\PublicLink;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @group Public links
 *
 * Endpoints for public download links.
 */
class PublicLinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Create public link.
     *
     * @responseFile 200 storage/responses/PublicLinkResponse.json
     *
     * @param \App\Http\Requests\v1\FileRequest $request
     * @return \App\Http\Resources\v1\PublicLinkResource
     */
    public function create(FileRequest $request): PublicLinkResource
    {
        return new PublicLinkResource(
            PublicLink::factory()->create([
                'user_id' => $request->user()->id,
                'path' => $request->path,
            ])
        );
    }

    /**
     * List public links.
     *
     * @responseFile 200 storage/responses/PublicLinkCollectionResponse.json
     *
     * @param Illuminate\Http\Request $request
     * @return App\Http\Resources\v1\PublicLinkPublicLinkCollection
     */
    public function index(Request $request): PublicLinkCollection
    {
        return new PublicLinkCollection(
            $request->user()->publicLinks->all()
        );
    }

    /**
     * Delete public link.
     *
     * @response 204
     *
     * @param \App\Models\PublicLink $link
     * @return Illuminate\Http\Response
     */
    public function destroy(PublicLink $link): Response
    {
        $link->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
