<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;

class CreateDirectoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'directory' => ['required', 'string', 'min:3', 'max:128', 'regex:/^[A-Za-z0-9\.\-\_\ ]{3,128}$/'],
        ];
    }

    /**
     * Parameters descriptions for Scribe.
     *
     * @return array<string, mixed>
     */
    public function queryParameters(): array
    {
        return [
            'directory' => [
                'description' => 'Directory name.',
                'example' => 'MyFolder'
            ],
        ];
    }
}
